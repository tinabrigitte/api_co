Hi there, my name is Tina Samson I'm a junior python/django developer.
This app I'm creating here is an api with the nexts endpoints:

1) GET /character/<id>/
2) POST /character/<id>/rating/

For the first endpoint I need to consult the api of https://swapi.dev/, this is an api with data of the Star Wars characters and I need to return:

	1. The fields "Homeworld" and "species_name"
	2. From 2) you can calculate the fields "average_rating" and "max_rating".

For the second endpoint, It receives a score from one to five and the id of the character. This data has to be saved in a BD.